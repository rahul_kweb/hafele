﻿using Hafele.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Hafele.Controllers
{
    public class HomeController : Controller
    {
        Utility util = new Utility();
        // GET: Home
        public ActionResult Index(string utm_source,string utm_medium, string utm_content,string utm_campaign,string Device,string City)
        {
            // Add Product in Checkbox list

            List<SelectListItem> items = new List<SelectListItem>()
            {
                 new SelectListItem { Value="Klara Highline Murphy Red", Text = "Klara Highline Murphy Red",  Selected= false},
                 new SelectListItem{Value="Klara Highline Grey" , Text = "Klara Highline Grey", Selected = false},
                 new SelectListItem{Value="Viola Pro" , Text = "Viola Pro", Selected = false},
                 new SelectListItem{Value="Magnus Cold Press Juicer" , Text = "Magnus Cold Press Juicer", Selected = false},
                 new SelectListItem{Value="OktaBlend" , Text = "OktaBlend", Selected = false},
                 new SelectListItem{Value="Dome Kettle Red" , Text = "Dome Kettle Red", Selected = false},
                 new SelectListItem{Value="Dome Kettle Grey" , Text = "Dome Kettle Grey", Selected = false},
                 new SelectListItem{Value="Queen Kettle" , Text = "Queen Kettle", Selected = false},
                 new SelectListItem{Value="Amber 2 Slot Toaster Opal" , Text = "Amber 2 Slot Toaster Opal", Selected = false},
                 new SelectListItem{Value="Amber 2 Slot Toaster Jade" , Text = "Amber 2 Slot Toaster Jade", Selected = false}

            };

            ViewBag.Offername = items;


            // Get Utm Parameters Value
            if (utm_source == null)
            {
                TempData["utm_source"] = "";
            }
            else
            {
                TempData["utm_source"] = utm_source;
            }

            if (utm_source == null)
            {
                TempData["utm_medium"] = "";
            }
            else
            {
                TempData["utm_medium"] = utm_medium;
            }

            if (utm_source == null)
            {
                TempData["utm_content"] = "";
            }
            else
            {
                TempData["utm_content"] = utm_content;
            }

            if (utm_source == null)
            {
                TempData["utm_campaign"] = "";
            }
            else
            {
                TempData["utm_campaign"] = utm_campaign;
            }

            if (utm_source == null)
            {
                TempData["Device"] = "";
            }
            else
            {
                TempData["Device"] = Device;
            }

            if (utm_source == null)
            {
                TempData["utm_city"] = "";
            }
            else
            {
                TempData["utm_city"] = City;
            }

          

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(OffersLeads obj, FormCollection fobj)
        {
       
            try
            {
                if (ModelState.IsValid)
                {

                obj.Offers = fobj["hiddenOffers"].ToString();

                    // remove comma from end of string
                    if (obj.Offers.EndsWith(","))
                    {
                        obj.Offers = obj.Offers.Substring(0, obj.Offers.Length - 1);
                    }


                    obj.utm_source= TempData["utm_source"].ToString();
                    obj.utm_medium = TempData["utm_medium"].ToString();
                    obj.utm_content = TempData["utm_content"].ToString();
                    obj.utm_campaign = TempData["utm_campaign"].ToString();
                    obj.Device = TempData["Device"].ToString();
                    obj.utm_city = TempData["utm_city"].ToString();


                    if (obj.Id == 0)
                    {
                        if (SaveOffers(obj))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ThankYou");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Index");
                        }
                    }

                }
                    return View("Index", obj);
                }

                catch (Exception ex)
                {
                    LogError log = new LogError();
                    log.HandleException(ex);

                    ViewBag.Message = "error";
                    ViewBag.Message1 = "Try again after some time.";

                    TempData["Message"] = ViewBag.Message;
                    TempData["Message1"] = ViewBag.Message1;
                    return RedirectToAction("Index");
                }

            
        }


        public bool SaveOffers(OffersLeads obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_OffersLeads"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@FullName", obj.FullName);
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@MobileNo", obj.MobileNo);
                cmd.Parameters.AddWithValue("@City", obj.City);
                cmd.Parameters.AddWithValue("@Offers", obj.Offers);
                cmd.Parameters.AddWithValue("@utm_source", obj.utm_source);
                cmd.Parameters.AddWithValue("@utm_medium", obj.utm_medium);
                cmd.Parameters.AddWithValue("@utm_content", obj.utm_content);
                cmd.Parameters.AddWithValue("@utm_campaign", obj.utm_campaign);
                cmd.Parameters.AddWithValue("@Device", obj.Device);
                cmd.Parameters.AddWithValue("@utm_city", obj.utm_city);


                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiry </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>FULL NAME : </strong></td><td> " + obj.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>EMAIL ID  : </strong></td> <td> " + obj.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>MOBILE NO. : </strong></td> <td> " + obj.MobileNo + " </td> </tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>CITY : </strong></td> <td> " + obj.City + " </td> </tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>OFFERS : </strong></td> <td> " + obj.Offers + " </td> </tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["ContactUsEmail"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Contact US", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PopupOffers(OffersLeads obj, FormCollection fobj)
        {
            try
            {
                if (ModelState.IsValid)
                {

                  obj.Offers= fobj["hiddenOffersPop"].ToString();

                    // remove comma from end of string

                    if (obj.Offers.EndsWith(","))
                    {
                        obj.Offers = obj.Offers.Substring(0, obj.Offers.Length-1);
                    }


                    obj.utm_source = TempData["utm_source"].ToString();
                    obj.utm_medium = TempData["utm_medium"].ToString();
                    obj.utm_content = TempData["utm_content"].ToString();
                    obj.utm_campaign = TempData["utm_campaign"].ToString();
                    obj.Device = TempData["Device"].ToString();
                    obj.utm_city = TempData["utm_city"].ToString();


                    if (obj.Id == 0)
                    {
                        if (SaveOffers(obj))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ThankYou");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Index");
                        }
                    }

                }
                return View("Index", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Index");
            }
        }


        public ActionResult ThankYou()
        {
            return View();
        }
        }
    }
