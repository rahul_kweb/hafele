﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace Hafele.Models
{
    public class Utility
    {
        #region Connection
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);
        #endregion

        #region Execute Query
        public bool Execute(SqlCommand cmd)
        {
            try
            {
               
                con.Open();
                cmd.Connection = con;
                int i = cmd.ExecuteNonQuery();
                return (i > 0);
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return false;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Table by command
        public DataTable Display(SqlCommand cmd) //using overloading methord
        {
            try
            {
                con.Open();
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd); //get data into adapter

                DataTable dt = new DataTable();
                da.Fill(dt);    //store into table
                return dt;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Table by query
        public DataTable Display(string query) //using overloading methord
        {
            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

                DataTable dt = new DataTable();
                da.Fill(dt);    //store into table

                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Set by command
        public DataSet Display1(SqlCommand cmd) //using overloading methord
        {
            try
            {
                con.Open();
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd); //get data into adapter

                DataSet ds = new DataSet();
                da.Fill(ds);    //store into table
                return ds;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Set by query
        public DataSet Display1(string query) //using overloading methord
        {
            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

                DataSet ds = new DataSet();
                da.Fill(ds);    //store into table

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Send Email
        public bool SendEmail(string Emailbody, string[] Toemailids, string Subject, string FilePath, Stream input)
        {
            bool result = false;
            #region Send Mail
            try
            {
                string EmailUserName = ConfigurationManager.AppSettings["EmailUsername"].ToString();
                string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
                string EmailHost = ConfigurationManager.AppSettings["EmailHost"].ToString();
                string EmailPort = ConfigurationManager.AppSettings["EmailPort"].ToString();
                bool EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"].ToString());
                string subject = Subject;

                string ToEmailid = string.Empty;
                for (int i = 0; i < Toemailids.Length; i++)
                {
                    ToEmailid += Toemailids[i] + ",";
                }
                if (ToEmailid != "")
                {
                    ToEmailid = ToEmailid.Substring(0, ToEmailid.Length - 1);
                }

                string toEmail = ToEmailid;
                string body = Emailbody;


                System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(EmailUserName, "Hafele");
                // Recipient e-mail address.
                Msg.To.Add(toEmail);
                Msg.Subject = subject;
                Msg.Body = body;
                Msg.IsBodyHtml = true;

                //file upload
                if (FilePath != "")
                {
                    Attachment attach = new Attachment(input, FilePath);
                    Msg.Attachments.Add(attach);
                }


                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();

                smtp.Host = EmailHost;

                smtp.Port = int.Parse(EmailPort);
                // smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(EmailUserName, EmailPassword);

                smtp.EnableSsl = EnableSsl;
                //smtp.UseDefaultCredentials = true;

                smtp.Send(Msg);
                // strSuccess = "Success";

                result = true; //success
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                result = false; //failed
                //  ViewBag.Error = "Try again after some time.";
                ///return View();
            }

            return result;
            #endregion
        }
        #endregion

        #region Export To Excel

        public void ExportToExcel(DataTable dt, string filename)
        {
            HttpResponse response = System.Web.HttpContext.Current.Response;

            // first let's clean up the response.object
            response.Clear();
            response.Charset = "";

            // set the response mime type for excel
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + ".xls\"");

            // create a string writer
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // instantiate a datagrid
                    System.Web.UI.WebControls.GridView gdvContact = new System.Web.UI.WebControls.GridView();
                    gdvContact.DataSource = dt;
                    gdvContact.DataBind();
                    gdvContact.HeaderRow.Font.Bold = true;


                    gdvContact.RenderControl(htw);
                    response.Write(sw.ToString());
                    response.End();
                }
            }
        }

        #endregion

        #region Upload Files Extentions
        public string GetUniqueName(string path, string initial, string ext, System.Web.UI.Page page, bool returnExtension)
        {
            //string uniquePart = Guid.NewGuid().ToString().Substring(0, 18);
            string uniquePart = DateTime.Now.ToFileTime().ToString().Substring(0, 18);
            string filename = string.Format("{0}{1}-{2}{3}", path, initial, uniquePart, ext);
            while (Directory.Exists(filename))
            {
                //uniquePart = Guid.NewGuid().ToString().Substring(0, 18);
                uniquePart = DateTime.Now.ToFileTime().ToString().Substring(0, 18);
                filename = string.Format("{0}{1}-{2}{3}", path, initial, uniquePart, ext);
            }
            if (returnExtension)
            {
                return string.Format("{0}-{1}{2}", initial, uniquePart, ext);
            }
            else
            {
                return string.Format("{0}-{1}", initial, uniquePart);
            }
        }

        public string GetUniqueName(string path, string initial, string ext, System.Web.UI.Page page)
        {
            return GetUniqueName(path, initial, ext, page, true);
        }


        public string Slugify(string phrase, int maxLength)
        {
            string str = RemoveAccent(phrase).ToLower();

            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");                      // REMOVE INVALID CHARS
            str = Regex.Replace(str, @"\s+", " ").Trim();                       // CONVERT MULTIPLE SPACES INTO ONE SPACE
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();  // CUT AND TRIM
            str = Regex.Replace(str, @"\s", "-");                               // CONVERT SPACE INTO HYPHEN

            return str;
        }

        public string Slugify(string phrase)
        {
            int maxLength = 200;
            return Slugify(phrase, maxLength);
        }

        public string RemoveAccent(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public bool IsNumeric(string strToCheck)
        {
            return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
        }

        public bool IsValidImageFileExtension(string extension)
        {
            return (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".bmp");
        }
        public bool IsValidPDFFileExtension(string extension)
        {
            return (extension == ".pdf");
        }
        public bool IsValidResumeFileExtension(string extension)
        {
            return (extension == ".pdf" || extension == ".doc" || extension == ".docx");
        }
        public bool IsValidPPTFileExtension(string extension)
        {
            return (extension == ".ppt" || extension == ".pptx");
        }
        public bool IsValidExcelFileExtension(string extension)
        {
            return (extension == ".xls" || extension == ".xlsx");
        }

        #endregion
    }
}