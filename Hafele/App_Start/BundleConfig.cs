﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Hafele.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region  Login Page css & js
            bundles.Add(new StyleBundle("~/Login/css").Include(
                            "~/Content/vendors/bootstrap/dist/css/bootstrap.min.css",
                            "~/Content/vendors/font-awesome/css/font-awesome.min.css",
                            "~/Content/vendors/animate.css/animate.min.css",
                            "~/Content/build/css/custom.min.css"));

            bundles.Add(new StyleBundle("~/Login/css1").Include(
                         "~/Content/Resource/Css/Login.css"));

            bundles.Add(new StyleBundle("~/css2").Include(
                         "~/Content/vendors/nprogress/nprogress.css"));

            bundles.Add(new StyleBundle("~/css3").Include(
                         "~/Content/vendors/google-code-prettify/bin/prettify.min.css"));

            bundles.Add(new StyleBundle("~/css4").Include(
                //"~/Content/vendors/bootstrap/dist/css/bootstrap.min.css",
                "~/Content/Resource/Website/Css/bootstrap.min.css",
                 "~/Content/Resource/Website/Css/font-awesome.min.css",
                 "~/Content/vendors/animate.css/animate.min.css",
                  "~/Content/Resource/Website/Css/custom.min.css",
                       "~/Content/Resource/Website/Css/css.css",
                       "~/Content/Resource/Website/Css/battabee.css"
                       ));


            bundles.Add(new ScriptBundle("~/Login/js").Include(
                           "~/Content/vendors/jquery/dist/jquery.min.js",
                           "~/Content/Resource/js/Login.js"));

            bundles.Add(new ScriptBundle("~/ClientLoginjs").Include(
                           "~/Content/vendors/bootstrap/dist/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/Login").Include(
                         "~/Content/Resource/js/Login.js"));

            bundles.Add(new ScriptBundle("~/Dashboard").Include(
                            "~/Content/vendors/fastclick/lib/fastclick.js",
                           "~/Content/vendors/nprogress/nprogress.js",
                            "~/Content/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js",
                         "~/Content/vendors/jquery.hotkeys/jquery.hotkeys.js",
                         "~/Content/vendors/google-code-prettify/src/prettify.js",
                         "~/Content/build/js/custom.min.js"
                         ));

            #endregion

            #region  Super Admin css & js

            bundles.Add(new StyleBundle("~/Admin/css1").Include(
                           "~/Content/vendors/bootstrap/dist/css/bootstrap.min.css",
                           "~/Content/vendors/font-awesome/css/font-awesome.min.css",
                           "~/Content/vendors/nprogress/nprogress.css",
                           "~/Content/vendors/bootstrap-daterangepicker/daterangepicker.css",
                            "~/Content/build/css/custom.min.css"
                           ));


            bundles.Add(new StyleBundle("~/Admin/css2").Include(
           "~/Content/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css",
           "~/Content/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
           "~/Content/vendors/iCheck/skins/flat/green.css"
                  ));


            bundles.Add(new ScriptBundle("~/Admin/js1").Include(
                          "~/Content/vendors/jquery/dist/jquery.min.js"
                          ));




            bundles.Add(new ScriptBundle("~/Admin/js2").Include(
              "~/Content/vendors/bootstrap/dist/js/bootstrap.min.js",
                           "~/Content/vendors/fastclick/lib/fastclick.js",
                           "~/Content/vendors/nprogress/nprogress.js",
                           "~/Content/vendors/Chart.js/dist/Chart.min.js",
                           "~/Content/vendors/jquery-sparkline/dist/jquery.sparkline.min.js",
                            //"~/Content/vendors/Flot/jquery.flot.js",
                            //"~/Content/vendors/Flot/jquery.flot.pie.js",
                            //"~/Content/vendors/Flot/jquery.flot.time.js",
                            //"~/Content/vendors/Flot/jquery.flot.stack.js",
                            //"~/Content/vendors/Flot/jquery.flot.resize.js",
                            //"~/Content/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                            //"~/Content/vendors/flot-spline/js/jquery.flot.spline.min.js",
                            //"~/Content/vendors/flot.curvedlines/curvedLines.js",
                            "~/Content/vendors/DateJS/build/date.js",
                            "~/Content/vendors/moment/min/moment.min.js",
                            "~/Content/vendors/bootstrap-daterangepicker/daterangepicker.js",
                            "~/Content/build/js/custom.min.js",
                            "~/Content/vendors/datatables.net/js/jquery.dataTables.min.js",
                            "~/Content/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
                            "~/Content/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
                            "~/Content/vendors/bootstrap-daterangepicker/daterangepicker.js",
                            "~/Content/vendors/iCheck/icheck.min.js"
                           //"~/Content/scripts/jquery.validate.min.js",
                           //"~/Content/scripts/jquery.validate.unobtrusive.js",
                           //"~/Content/scripts/jquery.validate.unobtrusive.min.js",
                           //"~/Content/vendors/iCheck/icheck.min.js"
                           ));

            #endregion


            #region Form Validation js

            bundles.Add(new ScriptBundle("~/Admin/FormValidation").Include(
                "~/Content/scripts/jquery.validate.min.js",
                "~/Content/scripts/jquery.validate.unobtrusive.js",
                "~/Content/scripts/jquery.validate.unobtrusive.min.js"
                ));

            #endregion



            // Code removed for clarity.
            BundleTable.EnableOptimizations = true; //minified all css and js

        }
    }
}